use std::io::Write;

pub fn input_string(t: &str) -> String {
    print!("{}", t);
    let _ = std::io::stdout().flush();

    let mut s = String::new();
    std::io::stdin().read_line(&mut s).expect("You didn't enter a correct word.");

    return s;
}

pub fn parameter_input(argc: i8, n: i8, v: Vec<f64>) -> Vec<f64> {
    if n > argc {
        return v;
    }

    return parameter_input(argc, n+1, [ v, vec![input_string(&format!("{}. > ", (n))[..]).trim().parse::<f64>().unwrap()]].concat());
}

pub struct MathFunction {
    pub name: String,
    pub argc: i8,
    pub run: Box<Fn(Vec<f64>) -> String>
}

pub fn build_math_function(name: &str, argc: i8, run: Box<Fn(Vec<f64>) -> String>) -> MathFunction {
    return MathFunction {
        name: name.to_string(),
        argc,
        run
    };
}

pub fn math_functions() -> Vec<MathFunction> {
    return vec![
        build_math_function("add", 2, Box::new(|args| { 
            let mut e: f64 = 0.0;

            for x in args {
                e += x;
            }
            
            return format!("{}", e);
        })),

        build_math_function("mul", 2, Box::new(|args| { 
            let mut e: f64 = 1.0;

            for x in args {
                e *= x;
            }

            return format!("{}", e);
        })),

        build_math_function("div", 2, Box::new(|args| { 
            let mut e: f64 = 0.0;
            let mut c = 0;
            
            for x in args {
                if (e == 0.0 && x == 0.0) || (e == 0.0 && c == 0) { 
                    e += x;
                    c += 1;
                }
                else if x == 0.0 && c != 0 { 
                    println!("error: division by zero!"); 
                    std::process::exit(1) 
                }
                else {
                    e = e / x;
                    c += 1;
                }
            }
            
            return format!("{}", e);
        })),

        build_math_function("sub", 2, Box::new(|args| { 
            let mut e: f64 = 0.0;
            let mut c = 0;
            
            for x in args {
                if e == 0.0 && c == 0 {
                    e = x;
                    c += 1;
                }
                else {
                    e -= x;
                }
            }
            
            return format!("{}", e);
        })),

        build_math_function("sqrt", 1, Box::new(|args| {
            if args[0] < 0.0 {
                return format!("for set of ℂ ⇒ i{}", (args[0]*(-1.0)).sqrt());
            }

            return format!("{}", args[0].sqrt());
        }))
    ];
}