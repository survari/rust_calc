mod math;

fn main() {
    let functions = math::math_functions();
    println!("Following functions exist: ");

    for f in &functions {
        println!("  - {}", f.name);
    }

    println!("\nWhich function do you want to use?");
    let s = math::input_string("> ");
    println!("");
    
    for f in &functions {
        if s.contains(&f.name) {
            let mut nums: Vec<f64> = math::parameter_input(f.argc, 1, vec![]);

            println!("\nResult: {}", (f.run)(nums));
            std::process::exit(0);
        }
    }

    println!("Function not found.");
}